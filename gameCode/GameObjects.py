"""
	Purpose: Class to hold all in-game objects that have no control over the actual game structure/logic (only are in it)
"""
from random import randint

class Player:
	def __init__(self, playerNumber, isAI):
		self.playerNumber = playerNumber #Integer. This allows for changes to the game that would allow more than two players without major code change
		self.toads = []
		self.attackedLocations = [] #Locations of where we have attacked so we don't attack the same spot twice, locations are stored in a tuple: (x, y) over a list to save memory
		self.missedLocations = []
		self.isAI = isAI
		self.messagesFromAI = [] #A list of messages from AI to another player
		self.nextMoves = [] #Locations of next moves if AI is playing

	def playTurn(self, uiLogicHandler):
		"""
			This method plays turns automatically using AI.
		"""

		if uiLogicHandler.logicHandler.started:
			"""
				Automatically attack the other player
			"""
			hasAttacked = False
			while not hasAttacked:
				if len(self.nextMoves) > 0:
					location = self.nextMoves.pop(0) #Get x and y values from the computed next best moves (if they exist) and remove them from that list
					x = location[0]
					y = location[1]
				else:
					#x and y here are generated dynamically from the size of the grid. This means we can change the grid size without changing any other code
					x = randint(0, len(uiLogicHandler.logicHandler.grid[0])-1)
					y = randint(0, len(uiLogicHandler.logicHandler.grid)-1)

				if not uiLogicHandler.hasAttackedLocation((x, y)):
					if uiLogicHandler.gridButtonClick(uiLogicHandler.logicHandler.grid[y][x], x, y):
						uiLogicHandler.otherPlayer.messagesFromAI.append("One of your toads has been hit!") #Tell the human player we have hit them

						"""Compute next best moves"""
						nextBestMoves = {"x": [x, x+1, x-1], "y": [y, y-1, x+1]}
						for xCoordinate in nextBestMoves['x']:
							for yCoordinate in nextBestMoves['y']:
								if xCoordinate >= 0 and yCoordinate >= 0 and xCoordinate <= 9 and yCoordinate <= 9: #Make sure we don't try and attack outside of the grid
									self.nextMoves.append((xCoordinate, yCoordinate))

					hasAttacked = True
			return True
		else:
			"""
				Automatically place toads on the board
			"""
			for i in range(5):
				addedToad = False
				while not addedToad:
					direction = "horizontal" if randint(0,1) == 1 else "vertical"
					x = randint(0, len(uiLogicHandler.logicHandler.grid[0])-1)
					y = randint(0, len(uiLogicHandler.logicHandler.grid)-1)
					addedToad = uiLogicHandler.gridButtonClick(uiLogicHandler.logicHandler.grid[y][x], x, y, direction=direction) #Try to place a toad, when this doesn't return False we end our turn
				if uiLogicHandler.getNextToadLength() < 2:
					if uiLogicHandler.currentPlayer == uiLogicHandler.logicHandler.players[-1]: #If we're the last player to play start the game
						uiLogicHandler.logicHandler.started = True
					return True


class Toad:
	def __init__(self, length, initialX, initialY, direction, owner):
		self.length = length #Health/Length of the toad in total
		self.initialX = initialX #Position on grid (x_max = len(uiLogicHandler.logicHandler.grid[y])-length)
		self.initialY = initialY #Position on grid (y_max = len(uiLogicHandler.logicHandler.grid[y])-length)
		self.direction = direction #Direction leafpads go in from initial (measured in 'horizontal' and 'vertical')
		self.owner = owner
		self.visible = False #This becomes True once it is discovered by the opponent