"""
	Purpose: Handle logic relating to the actual game
"""

from gameCode import GameObjects
from gameCode.ui import UI

class Game:
	"""
		Purpose: Handle attributes/logic for actions relating to the (non-ui) gameboard including starting the UI gameboard
	"""
	def __init__(self):
		self.players = [GameObjects.Player(1, False)]
		self.started = False #Becomes true once each player has placed all of their ships and is ready to fire
		self.grid = [[[] for j in range(10)] for i in range(10)]

		UILogic(self) #Object to handle all UI related logic, so the 'ui' object can strictly be display related methods for easier readability

	def isGameOver(self):
		"""
			Check to see if the other player has reached 0 toads left.
			Returns the winner if won and False if not
		"""
		for player in self.players:
			if len(player.toads) == 0:
				return [winner for winner in self.players if winner != player][0] #Returns the other player as the winner

		return False

class UILogic:
	"""
		Purpose: Handle attributes/logic for actions relating to the (ui) gameboard
	"""
	def __init__(self, logicHandler):
		self.logicHandler = logicHandler
		self.ui = UI(self.logicHandler.grid, self) #Start the GUI

	def createPlayer(self, isAI):
		"""
			Creates a new player
			Does not return a value
		"""
		newPlayerNumber = len(self.logicHandler.players)+1
		self.logicHandler.players.append(GameObjects.Player(newPlayerNumber, isAI))

		if newPlayerNumber > 1:
			self.currentPlayer = self.logicHandler.players[0]
			self.otherPlayer = self.logicHandler.players[newPlayerNumber-1] #Subtract 1 because Python counts from 0

	def changeTurn(self):
		"""
			Swaps the currentPlayer/otherPlayer attribute's values, and notifies AI that it's its turn
			Does not return a value
		"""
		if self.currentPlayer == self.logicHandler.players[0]:
			self.currentPlayer = self.logicHandler.players[1]
			self.otherPlayer = self.logicHandler.players[0]
		else:
			self.currentPlayer = self.logicHandler.players[0]
			self.otherPlayer = self.logicHandler.players[1]

		if self.currentPlayer.isAI:
			self.currentPlayer.playTurn(self) #Notify the AI to play their turn
			self.changeTurn()

	def hasAttackedLocation(self, location):
		"""
			Checks if the player has already attacked here (used to avoid repeat attacks)
			Returns True if the current player has already attacked this location
			Location should be a tuple: (x, y)
		"""
		return location in self.currentPlayer.attackedLocations

	def getNextToadLength(self):
		"""
			Gets the length of the next toad to place according to how many have already been placed and original Battleship rules
			Returns the length as an integer
		"""
		toadLength = 5-len(self.currentPlayer.toads)

		#Add an exceptions to the rule, for there to be two, 3-long toads
		if toadLength == 2 and len(self.currentPlayer.toads) == 3:
			toadLength = 3
		elif toadLength == 1 and len(self.currentPlayer.toads) == 4:
			toadLength = 2

		return toadLength

	def placeToad(self, *args):
		"""
			Adds a toad to the board
			Returns true if the toad can be placed at that location
			Must be given all the arguments that the class 'Toad' uses
		"""
		for i in range(args[0]*2): #Run the loop over the list twice, first to verify we can place there, and second to place
			toad = GameObjects.Toad(*args)
			if i <= toad.length-1:
				if toad.direction == 'vertical':
					cell = self.logicHandler.grid[toad.initialY+i][toad.initialX]
				elif toad.direction == 'horizontal':
					cell = self.logicHandler.grid[toad.initialY][toad.initialX+i]
				else:
					return False #Invalid information supplied, tell the user it didn't work

				if self.toadExists(self.currentPlayer, cell):
					return False
			else:
				#i = i-toad.length, to compensate for the first loop
				if toad.direction == 'vertical':
					cell = self.logicHandler.grid[toad.initialY+(i-toad.length)][toad.initialX].append(toad)
				elif toad.direction == 'horizontal':
					self.logicHandler.grid[toad.initialY][toad.initialX+(i-toad.length)].append(toad)
		self.currentPlayer.toads.append(GameObjects.Toad(*args))
		return True

	def toadExists(self, player, cell):
		"""
			Checks if a player has a toad at a certain location
			Returns toad (equivalent to True) if one exists
			`player` should be a 'Player' object. `cell` should be a list from the second dimension of 'gameCode.GameBoard.Game.grid'
		"""

		if len(cell) > 0:
			for toad in cell:
				if toad.owner == player:
					return toad
		return False

	def countToadsLeft(self):
		"""
			Updates each players list of toads to remove all dead toads
			Does not return a value
		"""
		for player in self.logicHandler.players:
			for toad in player.toads:
				deadBlocks = 0 #If this is equal to toad.length the toad is dead
				for i in range(toad.length):
					if toad.direction == 'vertical':
						cell = self.logicHandler.grid[toad.initialY+i][toad.initialX]
					elif toad.direction == 'horizontal':
						cell = self.logicHandler.grid[toad.initialY][toad.initialX+i]

					if self.toadExists(player, cell).visible:
						deadBlocks += 1

					if deadBlocks >= toad.length:
						player.toads.remove(toad)

	def gridButtonClick(self, cell, x, y, **kwargs):
		"""
			Handles clicks from the GUI on the button grid. If the game has started attack, if not place a toad
			Returns True/toad depending on action performed (toad is treated as True in an if statement)
		"""
		if self.logicHandler.started:
			#Because the game has started issue an attack

			self.currentPlayer.attackedLocations.append((x, y))

			#Check for frog in cell - if not yours mark it as visible
			toad = self.toadExists(self.otherPlayer, cell)
			if toad:
				toad.visible = True
				return toad

			return False
			
		else:
			#Because the game has not yet started issue a placement request

			try:
				return self.placeToad(self.getNextToadLength(), x, y, kwargs['direction'], self.currentPlayer) #Adds toad to board with error checking, etc
			except IndexError:
				return False #Catch if the user is trying to place his ship off of the sides of the grid