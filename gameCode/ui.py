"""
	Purpose: Manage the UI for the game
"""

import tkinter as tk
import tkinter.messagebox as msgbox
from os import path, execl
from functools import partial #Allows me to pass aguments from buttons without wasting memory using lambdas
import sys

class UI:
	def __init__(self, grid, uiLogicHandler):
		self.uiLogicHandler = uiLogicHandler
		self.widgetsToClean = []
		self.drawGameUI()

	def drawGameUI(self):
		"""
			Draw the root window + label and start all the other drawing + updating
			Does not return a value
		"""

		self.gameWindow = tk.Tk() #Instantiate the Tk object
		self.gameWindow.title("Battletoads")
		self.gameWindow.resizable(width=False, height=False) #Disalow resizing

		"""
			Add images to memory.
			By using os.path.abspath I know this will work no matter what the working directory the user is in
		"""
		self.battletoadImage = tk.PhotoImage(file=path.abspath(__file__+"/../../assets/battletoad_small.png"))
		self.deadBattletoadImage = tk.PhotoImage(file=path.abspath(__file__+"/../../assets/battletoad_small_dead.png"))
		self.placeholderImage = tk.PhotoImage(file=path.abspath(__file__+"/../../assets/placeholder.png"))
		self.missedImage = tk.PhotoImage(file=path.abspath(__file__+"/../../assets/miss.png"))

		self.labelGameInfo = tk.Label(self.gameWindow) #A label to show game info
		self.labelGameInfo.grid(row=0, column=0, columnspan=len(self.uiLogicHandler.logicHandler.grid[0])+1)

		self.placementDirection = "horizontal"
		def switchDirectionButtonClick():
			"""
				Swap the placement direction between horizontal and vertical
				Does not return a value
			"""
			if self.placementDirection == "horizontal":
				self.placementDirection = "vertical"
			else:
				self.placementDirection = "horizontal"
			self.updateUI()

		self.switchDirectionButton = tk.Button(self.gameWindow, text="Switch toad placement direction", command=switchDirectionButtonClick)
		self.switchDirectionButton.grid(row=11, column=0, columnspan=10)

		self.gameSetup()
		self.gameWindow.withdraw()
		self.gameWindow.mainloop() #Show the window

	def gameSetup(self):
		"""
			Create and show the dialog asking for game settings
			Does not return a value
		"""
		def finsihConfig(isAI):
			"""
				Tells the uiLogicHandler to create a new player
				Does not return a value
				`isAI` should be a boolean
			"""
			self.uiLogicHandler.createPlayer(isAI)
			self.updateUI()
			self.gameWindow.deiconify()
			dialog.destroy()

		dialog = tk.Toplevel()
		dialog.title("New Game")
		dialog.resizable(width=False, height=False) #Disalow resizing
		dialog.protocol('WM_DELETE_WINDOW', lambda: self.gameWindow.destroy()) #Close the root window if this dialog is closed.
		                                                                       #I'm using a lambda because otherwise the command would be run when creating the event listener

		tk.Label(dialog, text="Play against a computer or a friend?").grid(row=0, column=0, columnspan=2)
		tk.Button(dialog, text="Computer", command=partial(finsihConfig, True)).grid(row=1, column=0)
		tk.Button(dialog, text="Friend", command=partial(finsihConfig, False)).grid(row=1, column=1)

	def checkGameOver(self):
		"""
			Asks uiLogicHandler to compute if the game is over. If it is display a message box
			Returns False if game is not over
		"""
		self.uiLogicHandler.countToadsLeft()
		winner = self.uiLogicHandler.logicHandler.isGameOver()
		if self.uiLogicHandler.logicHandler.started and winner:
			#Game is over and we have won!
			if msgbox.askyesno("Game Over", "Player %s won! Would you like to play again?" % (winner.playerNumber)):
				execv(sys.executable, sys.executable, *sys.argv) #Restart the game
			else:
				sys.exit(0) #Code 0 means everything worked

		return False

	def updateUI(self):
		"""
			Reload UI elements inlcuding the grid, labels, and buttons. Also handles the graphical elements of a gridButtonClick event
			Does not return a value
		"""
		if self.uiLogicHandler.logicHandler.started:
			self.uiLogicHandler.countToadsLeft()
			self.checkGameOver()
			self.labelGameInfo.configure(text="Current Turn: Player %s | Your Battletoads: %s afloat | Their Battletoads: %s afloat" % (self.uiLogicHandler.currentPlayer.playerNumber, len(self.uiLogicHandler.currentPlayer.toads), len(self.uiLogicHandler.otherPlayer.toads)))
			
			for i in range(len(self.uiLogicHandler.currentPlayer.messagesFromAI)):
				msgbox.showinfo("Message", self.uiLogicHandler.currentPlayer.messagesFromAI.pop(i)) #Print messages from AI and delete it.

			self.switchDirectionButton.grid_forget() #Once the game has started we don't need this
		else:
			self.labelGameInfo.configure(text="Place your battletoads.\nPlayer %s placing %s toads %sly." % (self.uiLogicHandler.currentPlayer.playerNumber, self.uiLogicHandler.getNextToadLength(), self.placementDirection))

		#Create/Update the grid of buttons (playing board)
		def gridButtonClick(cell, x, y):
			if self.uiLogicHandler.logicHandler.started and self.uiLogicHandler.hasAttackedLocation((x, y)): #Make sure we're not hitting twice
				msgbox.showwarning("Attack Placement Warning", "You have already attacked this location")
				return False

			if self.uiLogicHandler.gridButtonClick(cell, x, y, direction=self.placementDirection):
				if self.uiLogicHandler.getNextToadLength() < 2 and not self.uiLogicHandler.logicHandler.started:
					if self.uiLogicHandler.currentPlayer == self.uiLogicHandler.logicHandler.players[-1]:
						self.uiLogicHandler.logicHandler.started = True #If the last player in the game has finished placing their toads start the game

					self.uiLogicHandler.changeTurn() #If we've already placed all of our frogs down give the other player a turn
				elif self.uiLogicHandler.logicHandler.started:
					#In-game turn has been played, and they have hit the opponent's toad
					if not self.uiLogicHandler.otherPlayer.isAI:
						msgbox.showinfo("Attack Result", "You have hit!")
					self.uiLogicHandler.changeTurn()
				self.updateUI()
			else:
				if not self.uiLogicHandler.logicHandler.started:
					msgbox.showwarning("Ship Placement Warning", "You are unable to place your ship here")
				else:
					if not self.uiLogicHandler.otherPlayer.isAI:
						msgbox.showinfo("Attack Result", "You have missed!")
					self.uiLogicHandler.currentPlayer.missedLocations.append((x, y))

					self.uiLogicHandler.changeTurn()
					self.updateUI()

		#Get rid of old widgets
		for widget in self.widgetsToClean:
			widget.grid_forget()

		#Draw the grid
		for i in range(len(self.uiLogicHandler.logicHandler.grid)):
			row = self.uiLogicHandler.logicHandler.grid[i]

			for j in range(len(row)):
				cell = tk.Button(self.gameWindow, width=50, height=42)
				cell.configure(command=partial(gridButtonClick, row[j], j, i))

				if self.uiLogicHandler.toadExists(self.uiLogicHandler.currentPlayer, row[j]) and not self.uiLogicHandler.logicHandler.started:
					cell.configure(image=self.battletoadImage)
				elif self.uiLogicHandler.toadExists(self.uiLogicHandler.otherPlayer, row[j]) and self.uiLogicHandler.toadExists(self.uiLogicHandler.otherPlayer, row[j]).visible:
					cell.configure(image=self.deadBattletoadImage)
				elif (j, i) in self.uiLogicHandler.currentPlayer.missedLocations:
					cell.configure(image=self.missedImage)
				else:
					cell.configure(image=self.placeholderImage)

				self.widgetsToClean.append(cell)
				cell.grid(row=i+1, column=j+1)