"""
	Purpose: Entrypoint into the game (instantiate all required objects to play)
"""

from sys import version_info, exit
if version_info < (3, 3):
	print("You must at least have Python 3.3 to run Battletoads.\nYou are running Python %s.%s" % (version_info[0], version_info[1]))
	exit()

from gameCode.GameBoard import Game
Game()